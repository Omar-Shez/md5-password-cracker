#import md5 library
import md5 
#set counter to know which password you're on
counter = 1
#recieve the md5 hash
pass_in = raw_input("Enter the md5 hash ")
#set the password file
pwfile = raw_input("Enter the password file name ")
#try to open the file
try:
	pwfile = open(pwfile, "r")
#if something wrong print file not found
except:
	print "\nFile not found"
#if everything checks out then password is used
for password in pwfile:
	#hashes the password
	filemd5 = md5.new(password.strip()).hexdigest()
	print "trying password number %d: %s " % (counter, password.strip())
	
	counter += 1
	#compare the password 
	if pass_in == filemd5
		print "\nMatch found. \nPassword is: %s" % password
		break
#if all else fails 
else: print "\nPassword not found"